package manen.playtime;

import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.settings.elements.StringElement;
import net.labymod.utils.Consumer;
import net.labymod.utils.Material;
import org.lwjgl.Sys;

import java.util.List;

public class PlaytimeAddon extends LabyModAddon {

    @Override
    public void onEnable() {
        Playtime.playtime = new Playtime(Sys.getTime());
        getApi().registerModule(new PlaytimeModule());
    }

    @Override
    public void loadConfig() {
        if(!getConfig().has("haveSaved")) {
            System.out.println("Playtime config has never been saved yet.");
            getConfig().addProperty("haveSaved", "have already saved no need to worry bruh");
            return;
        }

        if(getConfig().has("format")) Playtime.playtime.setFormat(getConfig().get("format").getAsString());
        if(getConfig().has("yearFormat")) Playtime.playtime.setYearFormat(getConfig().get("yearFormat").getAsString());
        if(getConfig().has("monthFormat")) Playtime.playtime.setMonthFormat(getConfig().get("monthFormat").getAsString());
        if(getConfig().has("dayFormat")) Playtime.playtime.setDayFormat(getConfig().get("dayFormat").getAsString());
        if(getConfig().has("hourFormat")) Playtime.playtime.setHourFormat(getConfig().get("hourFormat").getAsString());
        if(getConfig().has("minFormat")) Playtime.playtime.setMinFormat(getConfig().get("minFormat").getAsString());
        if(getConfig().has("secFormat")) Playtime.playtime.setSecFormat(getConfig().get("secFormat").getAsString());
    }

    @Override
    protected void fillSettings(List<SettingsElement> list) {
        StringElement yearFormatSetting = new StringElement( "Year format", new ControlElement.IconData(Material.PAPER), Playtime.playtime.getYearFormat(), new Consumer<String>() {
            @Override
            public void accept(String accepted) {
                Playtime.playtime.setYearFormat(accepted);
                getConfig().addProperty("yearFormat", accepted);
                saveConfig();
            }
        });

        StringElement monthFormatSetting = new StringElement( "Month format", new ControlElement.IconData(Material.PAPER), Playtime.playtime.getMonthFormat(), new Consumer<String>() {
            @Override
            public void accept(String accepted) {
                Playtime.playtime.setMonthFormat(accepted);
                getConfig().addProperty("monthFormat", accepted);
                saveConfig();
            }
        });

        StringElement dayFormatSetting = new StringElement( "Day format", new ControlElement.IconData(Material.PAPER), Playtime.playtime.getDayFormat(), new Consumer<String>() {
            @Override
            public void accept(String accepted) {
                Playtime.playtime.setDayFormat(accepted);
                getConfig().addProperty("dayFormat", accepted);
                saveConfig();
            }
        });

        StringElement hourFormatSetting = new StringElement( "Hour format", new ControlElement.IconData(Material.PAPER), Playtime.playtime.getHourFormat(), new Consumer<String>() {
            @Override
            public void accept(String accepted) {
                Playtime.playtime.setHourFormat(accepted);
                getConfig().addProperty("hourFormat", accepted);
                saveConfig();
            }
        });

        StringElement minFormatSetting = new StringElement( "Minute format", new ControlElement.IconData(Material.PAPER), Playtime.playtime.getMinFormat(), new Consumer<String>() {
            @Override
            public void accept(String accepted) {
                Playtime.playtime.setMinFormat(accepted);
                getConfig().addProperty("minFormat", accepted);
                saveConfig();
            }
        });

        StringElement secFormatSetting = new StringElement( "Second format", new ControlElement.IconData(Material.PAPER), Playtime.playtime.getSecFormat(), new Consumer<String>() {
            @Override
            public void accept(String accepted) {
                Playtime.playtime.setSecFormat(accepted);
                getConfig().addProperty("secFormat", accepted);
                saveConfig();
            }
        });

        StringElement formatSetting = new StringElement( "Format", new ControlElement.IconData(Material.BOOK),Playtime.defaultFormat, new Consumer<String>() {
            @Override
            public void accept(String accepted) {
                Playtime.playtime.setFormat(accepted);
                getConfig().addProperty("format", accepted);
                saveConfig();
            }
        });

        list.add(yearFormatSetting);
        list.add(monthFormatSetting);
        list.add(dayFormatSetting);
        list.add(hourFormatSetting);
        list.add(minFormatSetting);
        list.add(secFormatSetting);
        list.add(formatSetting);
    }
}
