package manen.playtime;

import net.labymod.ingamegui.moduletypes.SimpleModule;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public class PlaytimeModule extends SimpleModule {

    @Override
    public String getControlName() {
        return "Playtime";
    }

    @Override
    public String getDisplayName() {
        return "Playtime";
    }

    @Override
    public String getDisplayValue() {
        return Playtime.playtime.getTimeElapsed() + "";
    }

    @Override
    public String getDefaultValue() {
        return Playtime.playtime.millisecondsToString(120319);
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.APPLE);
    }

    @Override
    public void loadSettings() {
    }

    @Override
    public String getSettingName() {
        return "playtime";
    }

    @Override
    public String getDescription() {
        return "Measures the time you spend playing";
    }

    @Override
    public int getSortingId() {
        return 0;
    }
}
