package manen.playtime;

import net.minecraftforge.event.world.NoteBlockEvent;
import org.lwjgl.Sys;

public class Playtime {

    public static final String defaultYearFormat = "%y% years ";
    public static final String defaultMonthFormat = "%mo% months ";
    public static final String defaultDayFormat = "%d% days ";
    public static final String defaultHourFormat = "%h% hours ";
    public static final String defaultMinFormat = "%mi% min ";
    public static final String defaultSecFormat = "%s% sec ";

    public static final String defaultFormat = "%y%mo%d%h%mi%s";

    public static Playtime playtime;

    private long startTime;

    private String yearFormat;
    private String monthFormat;
    private String dayFormat;
    private String hourFormat;
    private String minFormat;
    private String secFormat;

    private String format;

    private long cacheTime;
    private String cacheValue;

    public Playtime(long startTime) {
        this(startTime, defaultYearFormat, defaultMonthFormat, defaultDayFormat, defaultHourFormat, defaultMinFormat, defaultSecFormat, defaultFormat);
    }

    public Playtime(long startTime, String yearFormat, String monthFormat, String dayFormat, String hourFormat, String minFormat, String secFormat, String format) {
        this.startTime = startTime;
        this.yearFormat = yearFormat;
        this.monthFormat = monthFormat;
        this.dayFormat = dayFormat;
        this.hourFormat = hourFormat;
        this.minFormat = minFormat;
        this.secFormat = secFormat;
        this.format = format;
    }

    public void cache() {
        cacheValue = millisecondsToString(getMillisecondsElapsed());
        cacheTime = Sys.getTime();
    }

    public long getMillisecondsElapsed() {
        return Sys.getTime() - startTime;
    }

    public String getTimeElapsed() {
        if(Sys.getTime() - cacheTime >= 1000) cache();

        return cacheValue;
    }

    public String millisecondsToString(long ms) {
        long sec = ms / 1000;
        long min = sec / 60;
        long hour = min / 60;
        long day = hour / 24;
        long month = day / 30;
        long year = month / 12;

        sec -= min * 60;
        min -= hour * 60;
        hour -= day * 24;
        day -= month * 30;
        month -= year * 12;

        String text = format;

        String yearStr = year != 0 ? yearFormat.replace("%y%", year + "") : "";
        String monthStr = month != 0 ? monthFormat.replace("%mo%", month + "") : "";
        String dayStr = day != 0 ? dayFormat.replace("%d%", day + "") : "";
        String hourStr = hour != 0 ? hourFormat.replace("%h%", hour + "") : "";
        String minStr = min != 0 ? minFormat.replace("%mi%", min + "") : "";
        String secStr = sec != 0 ? secFormat.replace("%s%", sec + "") : "";

        if(year != 0) text = text.replace("%y", yearStr);
        else text = text.replace("%y", "");

        if(month != 0) text = text.replace("%mo", monthStr);
        else text = text.replace("%mo", "");

        if(day != 0) text = text.replace("%d", dayStr);
        else text = text.replace("%d", "");

        if(hour != 0) text = text.replace("%h", hourStr);
        else text = text.replace("%h", "");

        if(min != 0) text = text.replace("%mi", minStr);
        else text = text.replace("%mi", "");

        if(sec != 0) text = text.replace("%s", secStr);
        else text = text.replace("%s", "");

        return text;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getYearFormat() {
        if(yearFormat != null) return yearFormat;
        else return defaultYearFormat;
    }

    public void setYearFormat(String yearFormat) {
        this.yearFormat = yearFormat;
    }

    public String getMonthFormat() {
        if(monthFormat != null) return monthFormat;
        else return defaultMonthFormat;
    }

    public void setMonthFormat(String monthFormat) {
        this.monthFormat = monthFormat;
    }

    public String getDayFormat() {
        if(dayFormat != null) return dayFormat;
        else return defaultDayFormat;
    }

    public void setDayFormat(String dayFormat) {
        this.dayFormat = dayFormat;
    }

    public String getHourFormat() {
        if(hourFormat != null) return hourFormat;
        else return defaultHourFormat;
    }

    public void setHourFormat(String hourFormat) {
        this.hourFormat = hourFormat;
    }

    public String getMinFormat() {
        if(minFormat != null) return minFormat;
        else return defaultMinFormat;
    }

    public void setMinFormat(String minFormat) {
        this.minFormat = minFormat;
    }

    public String getSecFormat() {
        if(secFormat != null) return secFormat;
        else return defaultSecFormat;
    }

    public void setSecFormat(String secFormat) {
        this.secFormat = secFormat;
    }

    public String getFormat() {
        if(format != null) return format;
        else return defaultFormat;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}
